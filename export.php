<?php
ini_set('display_errors', 'on');

include('Freeagent-PHP/Freeagent.php');
include('config.php');
$client = new Freeagent($fa_id, $fa_secret);

function pre($obj) {
	echo '<pre>';
	print_r($obj);
	echo '</pre>';
}

function expensePath($expense, $attachment) {
	$month = substr($expense->dated_on, 0, 7);

	$monthpath = dirname(__FILE__) . '/downloads/expenses/'. $month;
	if (!file_exists($monthpath)) {
		mkdir($monthpath, 0777, true);
	}

	$filename = $expense->dated_on . ' - ' . $attachment->file_name;
	$filepath = $monthpath . '/' . $filename;
	return $filepath;
}

function transactionPath($account, $transaction, $attachment) {
	$month = substr($transaction->dated_on, 0, 7);
	$account_name = $account->bank_name . " - " . $account->name;

	$monthpath = dirname(__FILE__) . '/downloads/' . $account_name . '/'. $month;
	if (!file_exists($monthpath)) {
		mkdir($monthpath, 0777, true);
	}

	$filename = $transaction->dated_on . ' - ' . $attachment->file_name;
	$filepath = $monthpath . '/' . $filename;
	return $filepath;
}

function download($filepath, $attachment) {
	set_time_limit(0);

	echo "Download to $filepath ...";

	$fp = fopen ($filepath, 'w+');

	$ch = curl_init(str_replace(" ","%20", $attachment->content_src));
	curl_setopt($ch, CURLOPT_TIMEOUT, 50);
	// write curl response to file
	curl_setopt($ch, CURLOPT_FILE, $fp); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

	// This disables SSL certificate validation due to the Windows
	// PHP distribution not coming with the necessary options enabled.
	// If you are converned about this, you will need to set up your copy
	// of PHP by following the steps here:
	// https://stackoverflow.com/questions/28858351/php-ssl-certificate-error-unable-to-get-local-issuer-certificate
	// then removing the four lines below this comment.
	if (PHP_OS == "WINNT") {
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	}

	// get curl response
	$downloaded = curl_exec($ch);
    $error = curl_error($ch);
	curl_close($ch);
	fclose($fp);

	if (!$downloaded) {
        pre("Download failed: $error");
        exit;
    } else {
        echo "Done\n";
    }
}

if (isset($_GET['token'])) {

	$token = $_GET['token'];

	// set authentication token
	$client->setAccessToken($token);
	
    echo '
    	<h1>Fetching Expenses:</h1>';

	$page = 0;

	do {
	    $response = $client->get('expenses', array(
	    	'per_page' => 100,
	    	'page' => $page
		));

		foreach($response->expenses as $expense) {
			if (isset($expense->attachment)) {
	    		$attachment = $expense->attachment;
	    		if ($attachment != null) {
	    			echo '<p><a href="' . $attachment->content_src .'">' . $expense->description . '</a></p>';
	    			$filepath = expensePath($expense, $attachment);
	    			download($filepath, $attachment);
	    		}
	    	}
    	}

	    $page++;
	} while (count($response->expenses) == 100);
	
    echo '
    	<h1>Fetching Transactions:</h1>';
    
    $response = $client->get('bank_accounts');

    foreach ($response->bank_accounts as $account) {
    	echo '<h2>' . $account->name . '</h2>';
    	pre($account);
	    echo '<p>Transactions:</p>';

    	$page = 0;

    	do {
		    $response = $client->get('bank_transactions', array(
		    	'bank_account' => basename($account->url),
		    	'per_page' => 100,
		    	'page' => $page
	    	));

		    foreach ($response->bank_transactions as $transaction) {
		    	foreach($transaction->bank_transaction_explanations as $explanation) {
		    		if (isset($explanation->attachment)) {
			    		$attachment = $explanation->attachment;
			    		if ($attachment != null) {
			    			echo '<p><a href="' . $attachment->content_src .'">' . $explanation->description . '</a></p>';
			    			$filepath = transactionPath($account, $transaction, $attachment);
			    			download($filepath, $attachment);
			    		}
			    	}
		    	}
		    }
		    $page++;
		} while (count($response->bank_transactions) == 100);
    }

// we have a code
} else if (isset($_GET['code'])) {

	// now exchange the code for an access token - you should save this for future usage
	$accessToken = $client->getAccessToken($_GET['code'], 'http://localhost:8080/export.php');

	$token = $accessToken->access_token;

	header('Location: http://localhost:8080/export.php?token=' . $token);
	exit();

// no OAuth code set
} else {

	// get the authorisation url we need to pass customer to, passing the url you want them returned to (This url)
	$authoriseURL = $client->getAuthoriseURL('http://localhost:8080/export.php');
	
	header('Location: '.$authoriseURL);
	exit();

}
?>

Done.