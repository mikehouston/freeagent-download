Freeagent Attachment Downloader
===

A quick and dirty script to download all Freeagent attachments from trasactions and expenses.


You will need a Freeagent app id. You can obtain one by going to:
[https://dev.freeagent.com/apps](https://dev.freeagent.com/apps)

This should then be set in the `config.php` file. You can copy the `config-template.php` and fill
in the necessary values.


To run a local PHP server, use the command:

```
php -S localhost:8080
```

You can use a different port if you like, but the Oauth callback should be changed to match the url
you have set here.

Finally, visit [http://localhost:8080/](http://localhost:8080/) to start the download process.

The script will download all attachments from the account you authorize, and store them
under the current directory in `downloads/{account}/{month}/{attachment file}`

Acknowledgements
---

This script uses the Freeagent-PHP client from [https://github.com/thoughtco/Freeagent-PHP](https://github.com/thoughtco/Freeagent-PHP)

Feel free to use the code for any purpose, no warranty implied.

Code is licenced under the MIT license.
